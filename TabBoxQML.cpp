/********************************************************************
 TabBoxQML - Viewer for QML TabBox

Copyright (C) 2011 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "TabBoxQML.h"

#include <QtGui/QGraphicsObject>
#include <QtGui/QLabel>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QAction>
#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/QDeclarativeContext>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeView>
#include <kdeclarative.h>
#include <KDE/KDebug>
#include <KDE/KDesktopFile>
#include <KDE/KGlobal>
#include <KDE/KIcon>
#include <KDE/KIconEffect>
#include <KDE/KIconLoader>
#include <KDE/KLocalizedString>
#include <KDE/KService>
#include <KDE/KStandardDirs>

TabBoxQML::TabBoxQML()
    : view(new QDeclarativeView(this))
{
    view->setAttribute(Qt::WA_TranslucentBackground);
    view->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    foreach (const QString &importPath, KGlobal::dirs()->findDirs("module", "imports")) {
        view->engine()->addImportPath(importPath);
    }
    foreach (const QString &importPath, KGlobal::dirs()->findDirs("data", "kwin/tabbox")) {
        view->engine()->addImportPath(importPath);
    }
    ExampleClientModel *model = new ExampleClientModel(this);
    view->engine()->addImageProvider(QLatin1String("client"), new TabBoxImageProvider(model));
    KDeclarative kdeclarative;
    kdeclarative.setDeclarativeEngine(view->engine());
    kdeclarative.initialize();
    kdeclarative.setupBindings();
    view->rootContext()->setContextProperty("clientModel", model);
    view->rootContext()->setContextProperty("layoutModel", new LayoutModel(this));
    view->setSource(KStandardDirs::locate("data", "tabbox-qml/list.qml"));
    setCentralWidget( view );
    QAction* a = new QAction(this);
    a->setText( "Quit" );
    connect(a, SIGNAL(triggered()), SLOT(close()) );
    menuBar()->addMenu( "File" )->addAction( a );
}

TabBoxQML::~TabBoxQML()
{}

TabBoxImageProvider::TabBoxImageProvider(QAbstractListModel* model)
    : QDeclarativeImageProvider(QDeclarativeImageProvider::Pixmap)
    , m_model(model)
{
}

QPixmap TabBoxImageProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{
    bool ok = false;
    QStringList parts = id.split('/');
    const int index = parts.first().toInt(&ok);
    if (!ok) {
        return QDeclarativeImageProvider::requestPixmap(id, size, requestedSize);
    }
    QSize s(32, 32);
    if (requestedSize.isValid()) {
        s = requestedSize;
    }
    *size = s;
    QPixmap icon(KIcon(m_model->data(m_model->index(index), Qt::UserRole+3).toString()).pixmap(s));
    if (parts.size() > 2) {
        KIconEffect *effect = KIconLoader::global()->iconEffect();
        KIconLoader::States state = KIconLoader::DefaultState;
        if (parts.at(2) == QLatin1String("selected")) {
            state = KIconLoader::ActiveState;
        } else if (parts.at(2) == QLatin1String("disabled")) {
            state = KIconLoader::DisabledState;
        }
        icon = effect->apply(icon, KIconLoader::Desktop, state);
    }
    return icon;
}

ExampleClientModel::ExampleClientModel (QObject* parent)
    : QAbstractListModel (parent)
{
    QHash<int, QByteArray> roles;
    roles[Qt::UserRole] = "caption";
    roles[Qt::UserRole+1] = "minimized";
    roles[Qt::UserRole+2] = "desktopName";
    setRoleNames(roles);
    init();
}

ExampleClientModel::~ExampleClientModel()
{
}

void ExampleClientModel::init()
{
    QList<QString> applications;
    applications << "konqbrowser" << "KMail2" << "systemsettings" << "dolphin";

    foreach (const QString& application, applications) {
        KService::Ptr service = KService::serviceByStorageId("kde4-" + application + ".desktop");
        if (service) {
            m_nameList << service->entryPath();
        }
    }
}

QVariant ExampleClientModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    switch (role) {
    case Qt::DisplayRole:
    case Qt::UserRole:
        return KDesktopFile(m_nameList.at(index.row())).readName();
    case Qt::UserRole+1:
        return false;
    case Qt::UserRole+2:
        return i18nc("An example Desktop Name", "Desktop 1");
    case Qt::UserRole+3:
        return KDesktopFile(m_nameList.at(index.row())).readIcon();
    }
    return QVariant();
}

int ExampleClientModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_nameList.size();
}

LayoutModel::LayoutModel(QObject *parent)
    : QAbstractListModel(parent)
{
    QHash<int, QByteArray> roles;
    roles[Qt::UserRole] = "name";
    roles[Qt::UserRole+1] = "sourcePath";
    setRoleNames(roles);
    init();
}

LayoutModel::~LayoutModel()
{
}

void LayoutModel::init()
{
    QStringList layouts;
    layouts << "informative" << "compact" << "text" << "big_icons" << "small_icons";
    QStringList descriptions;
    descriptions << i18nc("Name for a window switcher layout showing icon, name and desktop", "Informative");
    descriptions << i18nc("Name for a window switcher layout showing only icon and name", "Compact");
    descriptions << i18nc("Name for a window switcher layout showing only the name", "Text");
    descriptions << i18nc("Name for a window switcher layout showing large icons", "Large Icons");
    descriptions << i18nc("Name for a window switcher layout showing small icons", "Small Icons");

    for (int i=0; i<layouts.size(); ++i) {
        const QString path = KStandardDirs::locate("data", "kwin/tabbox/" + layouts.at(i) + ".qml");
        if (!path.isNull()) {
            m_nameList << descriptions.at(i);
            m_pathList << path;
        }
    }
}

QVariant LayoutModel::data (const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    switch (role) {
    case Qt::DisplayRole:
    case Qt::UserRole:
        return m_nameList.at(index.row());
    case Qt::UserRole + 1:
        return m_pathList.at(index.row());
    }
    return QVariant();
}

int LayoutModel::rowCount (const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_nameList.size();
}

#include "TabBoxQML.moc"
